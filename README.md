# taxon
*class-free program entities*

Taxon is a small library that lets you be explicit about entities in your programs. Taxon is useful for programs which are built on data structures rather than classes. Taxon uses predicate functions to check the structure of objects to identify entities and distinguish between them.

# System requirements
Tested on Ruby 2.4.4 only.

# Installation
Taxon is available as a gem. It is namespaced under my username since I am currently the only user.

```shell
gem install taxon.benforeva
```

To use in your code:

```shell
require 'taxon'
```

To add to your Gemfile:

```shell
gem 'taxon.benforeva', require: 'taxon'
```

To include Taxon as a project dependency use a rake task to install it from a local directory inside your project directory. Place the gem in the local gem directory and add a rake task that installs the gem from this location.

```ruby
# file - Rakefile
# local_directory contains private gems
task :install_local_gems do
  system 'cd local_directory && gem install taxon'
end
```

Remember to run `rake install_local_gems` before beginning.

# Usage

## Built-in predicates
Taxon comes with functions for testing primitive data such as strings and numbers and compound data structures such as sequences.

```ruby
irb
require 'taxon'

data = 4
data + 1 if Taxon.number? data
=> 5
data = [3, 'p', 2]
data << 1 if Taxon.tuple? data
=> [3, 'p', 2, 1]
```

Taxon also has convenience predicates for testing compound data structures built from primitive data.

```ruby
irb
require 'taxon'

data = [3, 6, 2]
data.reduce(:+) if Taxon.numbers? data
=> 11
```

## Custom predicates
Taxon lets you specify new entities with the built-in predicate functions.

```ruby
# file - custom_demo.rb
require 'taxon'

module CustomDemo

  module Taxonomy
    include Taxon

    def couch?(data)
      record?(data, [:seats, :legs])
    end
  end
end
```

## Entity search
Taxon lets you search for matching entities within a module.

```ruby
require_relative 'custom_demo.rb'

data = {:seats => 3, :legs => 4}
Taxon.entity(data, CustomDemo)
=> "Couch"
```
