require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/taxon/primitives.rb'

class TestPrimitives < MiniTest::Test
  include Primitives

  def test_number_is_integer
    assert number? 45
  end

  def test_number_is_bignum
    assert number? 34.324
  end

  def test_number_is_fraction
    assert number? Rational(0.6)
  end

  def test_nothing_is_nil
    assert nothing? nil
  end

  def test_string_is_string_literal
    assert string? "hugs"
  end

  def test_symbol
    assert symbol? :hugs
  end

  def test_boolean
    assert boolean? false
    assert boolean? true
  end
end
