require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/taxon/constraints.rb'

class TestConstraints < MiniTest::Test
  include Constraints

  def test_upper_bounded_succeeds_with_no_bounds
    assert bounded?(34)
  end

  def test_bounded_works_for_numbers
    assert bounded?(34, 5, 98)
  end

  def test_bounded_succeeds_with_lower_bound_only
    assert bounded?(34, 5)
  end

  def test_bounded_succeeds_with_upper_bound_only
    assert bounded?(34, nil, 98)
  end

  def test_upper_bounded_suceeds_with_numbers
    assert upper_bounded?(34, 98)
  end

  def test_upper_bounded_succeeds_with_no_bounds
    assert upper_bounded?(34)
  end
end
