require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/taxon/guard.rb'
require_relative '../lib/taxon/primitives.rb'

class TestGuard < MiniTest::Test
  include Taxon::Guard, Primitives

  def test_predicate_accepts_method
    assert predicate?(self, :number?)
  end

  def test_predicates_accepts_array_of_methods
    assert predicates?(self, [:number?, :symbol?])
  end

  def test_predicates_accepts_empty_array
    assert predicates?(self, [])
  end

  def test_varying_predicates_accepts_method
    assert varying_predicates?(self, :number?)
  end

  def test_varying_predicates_accepts_array_of_methods
    assert varying_predicates?(self, [:number?, :symbol?])
  end

  def test_record_target_true_when_map_values_are_predicates
    assert record_target?(self, {:home => :string?, "name" => :nothing?})
  end
end
