require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/taxon/entity.rb'
require_relative '../lib/taxon/list.rb'

class TestEntity < MiniTest::Test
  include Entity

  def test_entity_returns_primitive_entities
    assert_equal "Symbol", entity(:test)
    assert_equal "Nothing", entity(nil)
    assert_equal "String", entity("test")
    assert_equal "Number", entity(3423)
  end

  def test_entity_returns_heterogeneous_collections
    assert_equal "EmptyTuple", entity([])
    assert_equal "AnyTuple", entity([3, 'p', nil])
    assert_equal "AnyRecord", entity({"key" => 213})
  end

  def test_entity_returns_module_specific_entities
    # FIXME: This returns Numbers instead of AnyList
    #   from the List module.
    assert_equal "AnyList", entity([4, 5], List)
  end

  def test_entity_succeeds_when_matching_predicate_passed
    assert entity?(543, :number?)
  end

  def test_entity_succeeds_when_matching_predicate_passed_with_module
    assert entity?([5, 4, 3], :numbers?, List)
  end

  def test_entity_fails_when_non_matching_predicate_passed
    refute entity?(543, :string?)
  end

  def test_entity_fails_when_non_predicate_passed
    refute entity?(543, 'not a predicate')
  end
end
