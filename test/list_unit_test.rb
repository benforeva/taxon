require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/taxon/list.rb'
require_relative '../lib/taxon/primitives.rb'

class TestList < MiniTest::Test
  include List, Primitives

  def test_detect_matching_elements_in_array
    assert has_matching_elements ["all", "strings", "here"]
    assert has_matching_elements [3, 2, 6, 9]
  end

  def test_has_matching_elements_false_if_empty
    assert has_matching_elements []
  end

  def test_has_matching_elements_false_if_class_differs
    refute has_matching_elements [3, 2.0]
  end

  def test_any_list_accepts_elements_with_similar_classes
    assert any_list?([3, 5, 1])
  end

  def test_any_list_rejects_elements_with_varying_classes
    refute any_list?([3, 5.5, 1])
  end

  def test_any_list_accepts_arrays_of_any_size
    assert any_list?([3])
  end

  def test_list_accepts_predicate_matching_all_elements
    assert list?([3, 5, 1], :number?)
  end

  def test_list_accepts_arrays_of_any_size
    assert list?([3], :number?)
  end

  def test_list_accepts_empty_array
    assert list?([], :number?)
  end

  def test_list_constructs_list_from_single_element
    assert_equal [3], list(3, :number?)
  end

  def test_list_constructs_empty_list_from_empty_array
    assert_equal [], list([], :number?)
  end

  def test_list_constructs_list_from_array
    assert_equal [3, 5, 1], list([3, 5, 1], :number?)
  end

  def test_list_constructs_empty_list_from_non_matching_element
    assert_equal [], list('test', :number?)
    assert_equal [], list(['test', 3], :number?)
  end
end

class TestPrimitiveLists < MiniTest::Test
  include List

  def test_numbers_is_true_for_array_of_numbers
    assert numbers?([3, 5.8])
  end

  def test_strings_is_true_for_array_of_strings
    assert strings?(["party", "late"])
  end

  def test_symbols_is_true_for_array_of_symbols
    assert symbols?([:party, :late])
  end

  def test_nothings_is_true_for_array_of_nothings
    assert nothings?([nil, nil])
  end
end
