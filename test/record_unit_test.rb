require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/taxon/record.rb'

class TestRecord < MiniTest::Test
  include Record

  def test_any_record_true_when_record_empty
    assert any_record? Hash.new
  end

  def test_any_record_true_when_map_key_is_symbol
    assert any_record?({name: "Coral"})
  end

  def test_any_record_true_when_map_key_is_string
    assert any_record?({"name" => "Coral"})
  end

  def test_any_record_false_when_map_key_is_neither_string_nor_symbol
    refute any_record?({4 => "Coral"})
  end

  def test_record_true_when_map_key_present
    assert record?({name: "Coral"}, :name)
  end

  def test_record_true_when_no_predicates_given
    assert record?({name: "Coral"})
  end

  def test_record_true_when_map_keys_present
    assert record?({name: "Coral", home: "Caroni"}, [:name, :home])
  end

  def test_record_true_when_record_target_matches_entries
    assert record?({name: "Coral", home: "Caroni"}, {name: :string?, home: :string?})
  end

  def test_record_false_when_record_target_does_not_match_entries
    refute record?({name: 324, home: "Caroni"}, {name: :string?, home: :string?})
  end
end
