require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/taxon/table.rb'

class TestTable < MiniTest::Test
  include Table

  @@list_of_records = [{name: "Coral"}, {home: "Caroni"}]
  @@list_of_hashes = [{name: "Coral"}, {4 => "test"}]
  @@name_table = [{fname: "John", lname: "Smith"}, {fname: "Joane", lname: "Smith"}]

  def test_tabular_accepts_empty_tuple
    assert tabular?([])
  end

  def test_tabular_accepts_tuple_with_any_records
    assert tabular?(@@list_of_records)
  end

  def test_tabular_does_not_accept_tuple_with_non_records
    refute tabular?(@@list_of_hashes)
  end

  def test_table_accepts_empty_tuple
    assert table?([])
  end

  def test_table_accepts_entities_matching_key_names
    assert table?(@@name_table, [:fname, :lname])
  end

  def test_table_does_not_accept_entities_not_matching_key_names
    refute table?(@@name_table, ["fname", "lname"])
  end

  def test_table_does_not_accept_entities_not_matching_targets
    refute table?(@@name_table, {fname: :number?, lname: :string?})
  end
end
