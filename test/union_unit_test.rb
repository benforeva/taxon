require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/taxon/union.rb'

class TestUnion < MiniTest::Test
  include Taxon::Union

  def test_union_accepts_anything_with_no_entities_specified
    assert union?("test")
    assert union?(nil)
  end

  def test_union_accepts_a_single_entity
    assert union?("test", :string?)
  end

  def test_union_acepts_multiple_entities
    assert union?("test", [:number?, :string?])
    assert union?(87, [:number?, :string?])
  end
end
