require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/taxon.rb'
require_relative '../lib/taxon/version.rb'

class TestGem < MiniTest::Test

  def test_gem_version
    assert_equal Taxon::VERSION, "0.9.0"
  end

  def test_primitive_entities_api
    assert_respond_to Taxon, :number?
    assert_respond_to Taxon, :nothing?
    assert_respond_to Taxon, :string?
    assert_respond_to Taxon, :symbol?
    assert_respond_to Taxon, :boolean?
  end

  def test_guard_entities_api
    assert_respond_to Taxon::Guard, :predicate?
    assert_respond_to Taxon::Guard, :predicates?
    assert_respond_to Taxon::Guard, :varying_predicates?
  end

  def test_tuple_entities_api
    assert_respond_to Taxon, :empty_tuple?
    assert_respond_to Taxon, :any_tuple?
    assert_respond_to Taxon, :tuple?
  end

  def test_list_entities_api
    assert_respond_to Taxon, :any_list?
    assert_respond_to Taxon, :list?
    assert_respond_to Taxon, :symbols?
    assert_respond_to Taxon, :numbers?
    assert_respond_to Taxon, :strings?
    assert_respond_to Taxon, :nothings?
  end

  def test_record_entities_api
    assert_respond_to Taxon, :any_record?
    assert_respond_to Taxon, :record?
  end

  def test_constraints_api
    assert_respond_to Taxon, :bounded?
    assert_respond_to Taxon, :upper_bounded?
  end

  def test_table_entities_api
    assert_respond_to Taxon, :tabular?
    assert_respond_to Taxon, :table?
  end

  def test_index_entities_api
    assert_respond_to Taxon::Index, :index?
    assert_respond_to Taxon::Index, :index?
    assert_respond_to Taxon::Index, :add
  end

  def test_entity_search_api
    assert_respond_to Taxon, :entity
    assert_respond_to Taxon, :entity?
  end

  def test_union_api
    assert_respond_to Taxon, :union?
  end
end
