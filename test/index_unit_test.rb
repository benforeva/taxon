require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/taxon/index.rb'
require_relative '../lib/taxon/primitives.rb'

class TestIndex < MiniTest::Test
  include Taxon::Index

  def setup
    @keys = [:fname, :lname]
    @index_from_keys = index(@keys)
    @predicates = {fname: :string?, lname: :string?}
    @index_from_preds = index(@predicates)
    @entry0 = {fname: "Cora", lname: "Candor"}
    @entry1 = {fname: "Fenwin", lname: "Candor"}
    @entry_id = {fname: "Cora", lname: "Candor", id: "129321"}
  end

  def test_index_creates_empty_tuple
    assert_equal [], @index_from_keys
  end

  def test_index_object_responds_to_index_keys
    assert_equal @keys, @index_from_keys.index_keys
    assert_equal @keys, @index_from_preds.index_keys
  end

  def test_index_object_responds_to_index_targets
    assert_equal nil, @index_from_keys.index_targets
    assert_equal @predicates, @index_from_preds.index_targets
  end

  def test_add_to_index_data_matching_targets
    @result = Taxon::Index.add(@index_from_preds, @entry0)
    assert_equal "Cora", @result[0][:fname]
  end

  def test_add_to_index_data_matching_keys
    @result = Taxon::Index.add(@index_from_keys, {fname: 9, lname: :culinary})
    assert_equal 9, @result[0][:fname]
  end

  def test_add_accepts_unique_items_only_for_targets_based_index
    Taxon::Index.add(@index_from_preds, @entry0)
    Taxon::Index.add(@index_from_preds, @entry0)
    assert_equal 1, @index_from_preds.size
  end

  def test_add_accepts_unique_items_only_for_keys_based_index
    Taxon::Index.add(@index_from_keys, @entry0)
    Taxon::Index.add(@index_from_keys, @entry0)
    assert_equal 1, @index_from_keys.size
  end

  def test_add_accepts_non_unique_items_for_targets_based_index
    Taxon::Index.add(@index_from_preds, @entry0)
    Taxon::Index.add(@index_from_preds, @entry1)
    assert_equal 2, @index_from_preds.size
  end

  def test_add_accepts_non_unique_items_for_keys_based_index
    Taxon::Index.add(@index_from_keys, @entry0)
    Taxon::Index.add(@index_from_keys, @entry1)
    assert_equal 2, @index_from_keys.size
  end

  def test_add_ignores_special_keys_for_uniqueness
    Taxon::Index.add(@index_from_keys, @entry0)
    Taxon::Index.add(@index_from_keys, @entry_id)
    assert_equal 1, @index_from_keys.size
  end

  def test_add_ignores_specified_keys_for_uniqueness
    Taxon::Index.add(@index_from_keys, @entry0)
    Taxon::Index.add(@index_from_keys, @entry1, [:fname])
    assert_equal 1, @index_from_keys.size
  end

  def test_index_accepts_constructor_output
    assert index?(@index_from_keys)
    assert index?(@index_from_preds)
  end

  def test_index_does_not_accept_arrays
    refute index?(Array.new)
  end
end
