require 'minitest/autorun'
require 'minitest/pride'
require_relative '../lib/taxon/tuple.rb'
require_relative '../lib/taxon/primitives.rb'

class TestTuple < MiniTest::Test
  include Tuple, Primitives

  def test_empty_tuple_is_empty_array
    assert empty_tuple? []
  end

  def test_any_tuple_must_descend_from_array
    refute any_tuple? 'p'
  end

  def test_any_tuple_has_any_elements
    assert any_tuple? ['p', :p]
  end

  def test_any_tuple_accepts_matching_elements
    assert any_tuple? [3, 4]
  end

  def test_tuple_is_any_tuple_without_entry_predicates
    assert tuple?([3, 'p', 1])
  end

  def test_tuple_must_descend_from_array
    refute tuple? 'p'
    refute tuple? Hash.new
  end

  def test_tuple_accepts_arrays_of_any_size
    assert any_tuple?([3])
  end

  def test_tuple_must_match_each_entry
    assert tuple?([3, 'p', 1], [:number?, :string?, :number?])
  end

  def test_tuple_can_match_any_number_of_ordered_entries
    assert tuple?([3, 'p', 1], :number?)
  end

  def test_tuple_fails_if_element_cannot_match_entry_predicate
    refute tuple?([3, 'p', 1], :symbol?)
  end
end
