require_relative 'lib/taxon/version.rb'

Gem::Specification.new do |s|
  s.name        = 'taxon.benforeva'
  s.version     = Taxon::VERSION
  s.date        = '2018-07-24'
  s.summary     = 'class-free programming for Ruby'
  s.description = "Taxon lets you write class-free Ruby programs that"\
                  " are extremely explicit about the entities they operate on."
  s.homepage    = "https://bitbucket.org/benforeva/taxon/src/master/"
  s.license     = "MIT"
  s.authors     = ['Andre Dickson']
  s.email       = 'taxon@andredickson.com'
  s.files       = Dir['lib/taxon.rb', 'lib/taxon/*']
end
