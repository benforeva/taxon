require_relative 'primitives.rb'
require_relative 'tuple.rb'
require_relative 'record.rb'
require_relative 'guard.rb'

module Entity

  module DefaultEntities
    include Primitives, Record, Tuple
    extend self
  end

  def entity?(data, pred, ns=DefaultEntities)
    Taxon::Guard.predicate?(ns, pred) ? ns.send(pred, data) : false
  end

  def entity(data, ns=DefaultEntities)
    to_entity_name entity_tests(ns).find{|i| ns.send(i, data)}
  end

  def entity_tests(ns=DefaultEntities)
    entity_format_methods(ns).map{|m| m.intern}
  end

  def entity_format_methods(ns)
    ns.public_instance_methods.select{|m| m.to_s.end_with?("?")}
  end

  def to_entity_name(method_name)
    if (method_name.is_a?(Symbol) || method_name.is_a?(String))
      method_name.to_s.split("_").map{|i| to_entity_word(i)}.join
    end
  end

  def to_entity_word(word)
    word.end_with?("?") ? word.gsub(/\?/, "").capitalize : word.capitalize
  end
end
