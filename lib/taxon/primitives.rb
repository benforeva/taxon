module Primitives

  def nothing?(data)
    data == nil
  end

  def number?(data)
    data.is_a? Numeric
  end

  def string?(data)
    data.is_a? String
  end

  def symbol?(data)
    data.is_a? Symbol
  end

  def boolean?(data)
    data.is_a?(TrueClass) || data.is_a?(FalseClass)
  end
end
