require_relative 'tuple.rb'
require_relative 'primitives.rb'
require_relative 'guard.rb'

module List
  include Tuple, Primitives
  extend self

  def has_matching_elements(seq)
    type = seq[0].class
    seq.all?{|i| i.is_a?(type)}
  end

  def any_list?(seq)
    any_tuple?(seq) && has_matching_elements(seq)
  end

  def list?(seq, pred=nil)
    if any_tuple?(seq)
      Taxon::Guard.predicate?(self, pred) ? seq.all?{|i| send(pred, i)} : any_list?(seq)
    else
      false
    end
  end

  def list(data, pred)
    if varying?(data, pred)
      send(pred, data) ? [data] : data
    else
      []
    end
  end

  def varying?(data, pred)
    if Taxon::Guard.predicate?(self, pred)
      def multiple?(data, pred)
        list?(data, pred)
      end
      send(pred, data) || send(:multiple?, data, pred)
    else
      false
    end
  end

  def symbols?(data)
    list?(data, :symbol?)
  end

  def numbers?(data)
    list?(data, :number?)
  end

  def strings?(data)
    list?(data, :string?)
  end

  def nothings?(data)
    list?(data, :nothing?)
  end
end
