module Constraints

  def bounded?(data, min=nil, max=nil)
    if ((min==nil)&&(max==nil))
      true
    elsif (min==nil)
      data <= max
    elsif (max==nil)
      data >= min
    else
      (data <= max) && (data >= min)
    end
  end

  def upper_bounded?(data, max=nil)
    max==nil ? true : data <= max
  end
end
