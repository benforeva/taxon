require_relative 'primitives.rb'
require_relative 'guard.rb'

module Record
  include Primitives

  def any_record?(data)
    Taxon::Guard.any_record?(data)
  end

  def record?(data, pred=nil)
    if Taxon::Guard.record_key?(pred)
      any_record?(data) && data.has_key?(pred)
    elsif Taxon::Guard.record_keys?(pred)
      any_record?(data) && pred.all?{|i| data.has_key?(i)}
    elsif Taxon::Guard.record_target?(self, pred)
      data.is_a?(Hash) &&
        pred.entries.all?{|k, v| data.has_key?(k) && send(v, data[k])}
    else
      any_record?(data)
    end
  end
end
