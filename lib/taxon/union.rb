require_relative 'primitives.rb'
require_relative 'guard.rb'

module Taxon
  module Union
    include Primitives

    def any?(data)
      data.is_a? Object
    end

    def union?(data, pred=:any?)
      if Taxon::Guard.varying_predicates?(self, pred)
        if Taxon::Guard.predicate?(self, pred)
          send(pred, data)
        else
          pred.any?{|i| send(i, data)}
        end
      else
        false
      end
    end
  end
end
