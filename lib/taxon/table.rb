require_relative 'tuple.rb'
require_relative 'record.rb'

module Table
  include Tuple, Record

  def tabular?(data)
    any_tuple?(data) &&
      data.all?{|i| any_record?(i)}
  end

  def table?(data, pred=nil)
    any_tuple?(data) &&
      data.all?{|i| record?(i, pred)}
  end
end
