module Taxon
  module Guard
    extend self

    def predicate?(rec, meth)
      meth.is_a?(Symbol) && rec.respond_to?(meth)
    end

    def predicates?(rec, meths)
      meths.is_a?(Array) && meths.all?{|m| predicate?(rec, m)}
    end

    def varying_predicates?(rec, meths)
      predicate?(rec, meths) || predicates?(rec, meths)
    end

    def record_key?(data)
      data.is_a?(Symbol) || data.is_a?(String)
    end

    def record_keys?(data)
      data.is_a?(Array) && data.all?{|i| record_key?(i)}
    end

    def record_target?(rec, data)
      any_record?(data) && Guard.predicates?(rec, data.values)
    end

    def any_record?(data)
      data.is_a?(Hash) && data.keys.all?{|i| record_key?(i)}
    end
  end
end
