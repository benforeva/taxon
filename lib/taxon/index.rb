require_relative 'record.rb'

# An array of records with set semantics
module Taxon
  module Index
    include Record

    def index(preds)
      ind = Array.new
      setup_index_keys(ind, preds)
      setup_index_targets(ind, preds)
      ind
    end

    def setup_index_keys(index, preds)
      if Guard.record_keys?(preds)
        index.define_singleton_method(:index_keys) {preds}
      elsif Guard.record_target?(self, preds)
        index.define_singleton_method(:index_keys) {preds.keys}
      end
    end

    def setup_index_targets(index, targets)
      if Guard.record_keys?(targets)
        index.define_singleton_method(:index_targets) {}
      elsif Guard.record_target?(self, targets)
        index.define_singleton_method(:index_targets) {targets}
      end
    end

    def index?(data)
      data.is_a?(Array) &&
        data.respond_to?(:index_keys) &&
          data.respond_to?(:index_targets)
    end

    def add(index, record, ignore=ignore_keys)
      if nothing?(index.index_targets)
        unique_insert(index, record, ignore) if record?(record, index.index_keys)
      else
        unique_insert(index, record, ignore) if record?(record, index.index_targets)
      end
    end

    def unique_insert(index, record, ignore=ignore_keys)
      index.all?{|e| is_unique_entry(e, record, ignore)} ? index.push(record) : index
    end

    def is_unique_entry(entry, record, ignore=ignore_keys)
      keys = (entry.keys - ignore).any?{|i| entry[i] != record[i]}
    end

    def ignore_keys
      [:uuid, :UUID, :id, :Id, :ID]
    end

    extend self
  end
end
