require_relative 'guard.rb'

module Tuple

  def empty_tuple?(seq)
    any_tuple?(seq) && seq.empty?
  end

  def any_tuple?(seq)
    seq.is_a?(Array)
  end

  def tuple?(tuple, entry_tests=[])
    if empty_tuple?(entry_tests)
      any_tuple?(tuple)
    elsif Taxon::Guard.varying_predicates?(self, entry_tests)
      if entry_tests.is_a? Array
        upper = [tuple.size, entry_tests.size].min
        (0...upper).all?{|i| send(entry_tests[i], tuple[i])}
      else
        send(entry_tests, tuple[0])
      end
    end
  end
end
