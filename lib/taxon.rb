require_relative 'taxon/primitives.rb'
require_relative 'taxon/guard.rb'
require_relative 'taxon/tuple.rb'
require_relative 'taxon/list.rb'
require_relative 'taxon/record.rb'
require_relative 'taxon/constraints.rb'
require_relative 'taxon/table.rb'
require_relative 'taxon/index.rb'
require_relative 'taxon/entity.rb'
require_relative 'taxon/union.rb'

module Taxon
  include Primitives, Tuple, Union, List, Record, Table, Constraints, Entity
  extend self
end
